import React from "react";
import "./searchFilter.css";
class SearchFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchTypeActive: false,
            searchByActive: false,
            searchForActive: false,
        };
    }

    handleDropDown = (e) => {
        e.preventDefault();
        let key = `${e.target.id}Active`;
        this.setState({
            searchTypeActive: false,
            searchByActive: false,
            searchForActive: false,
        });
        this.setState({
            [key]: !this.state[key],
        });
    };

    handleFilter = (e) => {
        this.setState({
            searchTypeActive: false,
            searchByActive: false,
            searchForActive: false,
        });
        let filterType = e.target.parentElement.parentElement.id;
        let filterBy = e.target.id;
        console.log(filterType, filterBy);
        this.props.filterChanged({ [filterType]: filterBy });
    };

    render() {
        return (
            <div className="SearchFilters_filters">
                <span className="search-filter-container">
                    Search
                    <div
                        className={"drop-down"}
                        onClick={(e) => {
                            this.handleDropDown(e);
                        }}
                        id="searchType"
                    >
                        {this.props.filterType.searchType}
                        <ul
                            className={`drop-down-list ${
                                this.state.searchTypeActive &&
                                "display-drop-down"
                            }`}
                            id="searchType"
                        >
                            <li>
                                <button
                                    id="All"
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        this.handleFilter(e);
                                    }}
                                >
                                    All
                                </button>
                            </li>
                            <li>
                                <button
                                    id="Stories"
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        this.handleFilter(e);
                                    }}
                                >
                                    Stories
                                </button>
                            </li>
                            <li>
                                <button
                                    id="Comments"
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        this.handleFilter(e);
                                    }}
                                >
                                    Comments
                                </button>
                            </li>
                        </ul>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            strokeWidth="2"
                            strokeLinecap="r<button>ound"
                            strokeLinejoin="round"
                        >
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                    </div>
                </span>
                <span className="search-filter-container">
                    by
                    <div
                        className="drop-down"
                        onClick={(e) => this.handleDropDown(e)}
                        id="searchBy"
                    >
                        {this.props.filterType.searchBy}

                        <ul
                            className={`drop-down-list ${
                                this.state.searchByActive && "display-drop-down"
                            }`}
                            id="searchBy"
                        >
                            <li>
                                <button
                                    id="Popularity"
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        this.handleFilter(e);
                                    }}
                                >
                                    Popularity
                                </button>
                            </li>
                            <li>
                                <button
                                    id="Date"
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        this.handleFilter(e);
                                    }}
                                >
                                    Date
                                </button>
                            </li>
                        </ul>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        >
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                    </div>
                </span>
                <span className="search-filter-container">
                    for
                    <div
                        className="drop-down"
                        onClick={(e) => this.handleDropDown(e)}
                        id="searchFor"
                    >
                        {this.props.filterType.searchFor}
                        <ul
                            className={`drop-down-list ${
                                this.state.searchForActive &&
                                "display-drop-down"
                            }`}
                            id="searchFor"
                        >
                            <li>
                                <button
                                    id="All Time"
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        this.handleFilter(e);
                                    }}
                                >
                                    All time
                                </button>
                            </li>
                            <li>
                                <button
                                    id="Last 24h"
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        this.handleFilter(e);
                                    }}
                                >
                                    Last 24h
                                </button>
                            </li>
                            <li>
                                <button
                                    id="Past Week"
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        this.handleFilter(e);
                                    }}
                                >
                                    Past Week
                                </button>
                            </li>
                            <li>
                                <button
                                    id="Past Month"
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        this.handleFilter(e);
                                    }}
                                >
                                    Past Month
                                </button>
                            </li>
                            <li>
                                <button
                                    id="Last Year"
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        this.handleFilter(e);
                                    }}
                                >
                                    Past Year
                                </button>
                            </li>
                        </ul>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        >
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                    </div>
                </span>
            </div>
        );
    }
}

export default SearchFilter;
