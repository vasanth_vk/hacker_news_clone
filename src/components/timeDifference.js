import moment from "moment";
const timeDifference = (createdTime) => {
    return moment.unix(createdTime).fromNow();
};

export default timeDifference;
