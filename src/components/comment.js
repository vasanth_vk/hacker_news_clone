import React from "react";
import "./article.css";
import timeDifference from "./timeDifference";

class Comment extends React.Component {
    render() {
        return (
            <article className="Story">
                <div className="Story_container">
                    <div className="Story_data">
                        <div className="Story_meta">
                            <span>
                                <a
                                    href={`https://news.ycombinator.com/user?id=${this.props.articleInfo.author}`}
                                >
                                    <span>{this.props.articleInfo.author}</span>
                                </a>
                            </span>
                            <span className="Story_separator">|</span>
                            <span>
                                <a
                                    href={`https://news.ycombinator.com/item?id=${this.props.articleInfo.objectID}`}
                                >
                                    {timeDifference(
                                        this.props.articleInfo.created_at_i
                                    )}
                                </a>
                            </span>
                            <span className="Story_separator">|</span>
                            <span className="Story_link">
                                <a
                                    href={`https://news.ycombinator.com/item?id=${this.props.articleInfo.parent_id}`}
                                >
                                    parent
                                </a>
                            </span>
                            <span className="Story_separator">|</span>
                            <span className="Story_link">
                                on:{" "}
                                <a
                                    href={`https://news.ycombinator.com/item?id=${this.props.articleInfo.story_id}`}
                                >
                                    <span>
                                        {this.props.articleInfo.story_title}
                                    </span>
                                </a>
                            </span>
                            <div className="Story_comment">
                                <span
                                    dangerouslySetInnerHTML={{
                                        __html: this.props.articleInfo
                                            .comment_text,
                                    }}
                                ></span>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        );
    }
}

export default Comment;
