import React from "react";
import "./article.css";
import timeDifference from "./timeDifference";

class Article extends React.Component {
    render() {
        return (
            <article className="Story">
                <div className="Story_container">
                    <div className="Story_data">
                        <div className="Story_title">
                            <a
                                href={
                                    this.props.articleInfo.objectID &&
                                    `https://news.ycombinator.com/item?id=${this.props.articleInfo.objectID}`
                                }
                            >
                                <span>{this.props.articleInfo.title}</span>
                            </a>
                            <a
                                href={`${this.props.articleInfo.url}`}
                                target="_blank"
                                className="Story_link"
                                rel="noopener noreferrer"
                            >
                                {this.props.articleInfo.url
                                    ? `(${this.props.articleInfo.url})`
                                    : ""}
                            </a>
                        </div>
                        <div className="Story_meta">
                            <span>
                                <a
                                    href={
                                        this.props.articleInfo.objectID &&
                                        `https://news.ycombinator.com/item?id=${this.props.articleInfo.objectID}`
                                    }
                                >
                                    {this.props.articleInfo.points} points
                                </a>
                            </span>
                            <span className="Story_separator">|</span>
                            <span>
                                <a
                                    href={`https://news.ycombinator.com/user?id=${this.props.articleInfo.author}`}
                                >
                                    <span>{this.props.articleInfo.author}</span>
                                </a>
                            </span>
                            <span className="Story_separator">|</span>
                            <span>
                                <a
                                    href={`https://news.ycombinator.com/item?id=${this.props.articleInfo.objectID}`}
                                >
                                    {timeDifference(
                                        this.props.articleInfo.created_at_i
                                    )}
                                </a>
                            </span>
                            <span className="Story_separator">|</span>
                            <span>
                                <a
                                    href={`https://news.ycombinator.com/item?id=${this.props.articleInfo.objectID}`}
                                >
                                    {`${this.props.articleInfo.num_comments} comments`}
                                </a>
                            </span>
                            {this.props.articleInfo.story_text && (
                                <div className="Story_comment">
                                    <span
                                        dangerouslySetInnerHTML={{
                                            __html: this.props.articleInfo
                                                .story_text,
                                        }}
                                    ></span>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </article>
        );
    }
}

export default Article;
