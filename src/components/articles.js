import React from "react";
import Article from "./article";
import Comment from "./comment";

class Articles extends React.Component {
    render() {
        return this.props.articles.map((article) => {
            return article.title ? (
                <Article key={article.objectID} articleInfo={article} />
            ) : (
                <Comment key={article.objectID} articleInfo={article} />
            );
        });
    }
}

export default Articles;
