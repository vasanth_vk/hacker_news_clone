import moment from "moment";

let last24h = moment().subtract(24, "hours").unix();
let pastweek = moment().subtract(1, "week").unix();
let pastmonth = moment().subtract(1, "month").unix();
let pastyear = moment().subtract(1, "year").unix();

const filterObj = {
    "All-Popularity-All Time": `https://hn.algolia.com/api/v1/search?tags=(story,comment)`,
    "Stories-Popularity-All Time": `https://hn.algolia.com/api/v1/search?tags=story`,
    "Comments-Popularity-All Time": `https://hn.algolia.com/api/v1/search?tags=comment`,

    "All-Date-All Time": `https://hn.algolia.com/api/v1/search_by_date?tags=(story,comment)`,
    "Stories-Date-All Time": `https://hn.algolia.com/api/v1/search_by_date?tags=story`,
    "Comments-Date-All Time": `https://hn.algolia.com/api/v1/search_by_date?tags=comment`,

    "All-Popularity-Last 24h": `https://hn.algolia.com/api/v1/search?tags=(story,comment)&numericFilters=created_at_i>${last24h}`,
    "All-Popularity-Past Week": `https://hn.algolia.com/api/v1/search?tags=(story,comment)&numericFilters=created_at_i>${pastweek}`,
    "All-Popularity-Past Month": `https://hn.algolia.com/api/v1/search?tags=(story,comment)&numericFilters=created_at_i>${pastmonth}`,
    "All-Popularity-Last Year": `https://hn.algolia.com/api/v1/search?tags=(story,comment)&numericFilters=created_at_i>${pastyear}`,

    "Stories-Popularity-Last 24h": `https://hn.algolia.com/api/v1/search?tags=story&numericFilters=created_at_i>${last24h}`,
    "Stories-Popularity-Past Week": `https://hn.algolia.com/api/v1/search?tags=story&numericFilters=created_at_i>${pastweek}`,
    "Stories-Popularity-Past Month": `https://hn.algolia.com/api/v1/search?tags=story&numericFilters=created_at_i>${pastmonth}`,
    "Stories-Popularity-Last Year": `https://hn.algolia.com/api/v1/search?tags=story&numericFilters=created_at_i>${pastyear}`,

    "Comments-Popularity-Last 24h": `https://hn.algolia.com/api/v1/search?tags=comment&numericFilters=created_at_i>${last24h}`,
    "Comments-Popularity-Past Week": `https://hn.algolia.com/api/v1/search?tags=comment&numericFilters=created_at_i>${pastweek}`,
    "Comments-Popularity-Past Month": `https://hn.algolia.com/api/v1/search?tags=comment&numericFilters=created_at_i>${pastmonth}`,
    "Comments-Popularity-Last Year": `https://hn.algolia.com/api/v1/search?tags=comment&numericFilters=created_at_i>${pastyear}`,

    "All-Date-Last 24h": `https://hn.algolia.com/api/v1/search_by_date?tags=(story,comment)&numericFilters=created_at_i>${last24h}`,
    "All-Date-Past Week": `https://hn.algolia.com/api/v1/search_by_date?tags=(story,comment)&numericFilters=created_at_i>${pastweek}`,
    "All-Date-Past Month": `https://hn.algolia.com/api/v1/search_by_date?tags=(story,comment)&numericFilters=created_at_i>${pastmonth}`,
    "All-Date-Last Year": `https://hn.algolia.com/api/v1/search_by_date?tags=(story,comment)&numericFilters=created_at_i>${pastyear}`,

    "Stories-Date-Last 24h": `https://hn.algolia.com/api/v1/search_by_date?tags=story&numericFilters=created_at_i>${last24h}`,
    "Stories-Date-Past Week": `https://hn.algolia.com/api/v1/search_by_date?tags=story&numericFilters=created_at_i>${pastweek}`,
    "Stories-Date-Past Month": `https://hn.algolia.com/api/v1/search_by_date?tags=story&numericFilters=created_at_i>${pastmonth}`,
    "Stories-Date-Last Year": `https://hn.algolia.com/api/v1/search_by_date?tags=story&numericFilters=created_at_i>${pastyear}`,

    "Comments-Date-Last 24h": `https://hn.algolia.com/api/v1/search_by_date?tags=comment&numericFilters=created_at_i>${last24h}`,
    "Comments-Date-Past Week": `https://hn.algolia.com/api/v1/search_by_date?tags=comment&numericFilters=created_at_i>${pastweek}`,
    "Comments-Date-Past Month": `https://hn.algolia.com/api/v1/search_by_date?tags=comment&numericFilters=created_at_i>${pastmonth}`,
    "Comments-Date-Last Year": `https://hn.algolia.com/api/v1/search_by_date?tags=comment&numericFilters=created_at_i>${pastyear}`,
};
export default filterObj;
