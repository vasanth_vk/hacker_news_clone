import React from "react";
import "./App.css";
import Articles from "./components/articles";
import Header from "./components/header";
import Loader from "./components/loader";
import SearchFilter from "./components/searchFilter";
import fetchArticles from "./fetchArticles";
import filterObj from "./filterObj";
import deepEqual from "deep-equal";
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            articlesArray: [],
            isLoading: true,
            myFilter: {
                searchType: "All",
                searchBy: "Popularity",
                searchFor: "All Time",
            },
        };
    }

    handleSearchQuery = (searchQuery) => {
        this.setState((prevState) => {
            console.log(prevState);
            return { isLoading: true };
        });
        this.setState({ isLoading: true });
        let url = filterObj[Object.values(this.state.myFilter).join("-")];
        url = url.split("?");
        url[0] += `?query=${searchQuery}`;
        url = url.join("&");
        if (searchQuery) {
            fetchArticles(url)
                .then((data) =>
                    this.setState({
                        isLoading: false,
                        articlesArray: data.hits,
                    })
                )
                .catch((err) => console.log(err));
        } else {
            fetchArticles(
                filterObj[Object.values(this.state.myFilter).join("-")]
            )
                .then((data) => {
                    this.setState({
                        isLoading: false,
                        articlesArray: data.hits,
                    });
                })
                .catch((err) => console.log(err));
        }
    };

    handleFilter = (filterBy) => {
        if (
            !deepEqual(
                this.state.myFilter,
                Object.assign({}, this.state.myFilter, filterBy)
            )
        ) {
            this.setState(Object.assign(this.state.myFilter, filterBy));
            let url = filterObj[Object.values(this.state.myFilter).join("-")];
            this.setState({ isLoading: true });

            fetchArticles(url)
                .then((data) => {
                    this.setState({
                        articlesArray: data.hits,
                        isLoading: false,
                    });
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    };
    componentDidMount() {
        this.setState({ isLoading: true });
        fetchArticles(
            "https://hn.algolia.com/api/v1/search?tags=(story,comment)"
        )
            .then((data) => {
                this.setState({ articlesArray: data.hits, isLoading: false });
            })
            .catch((err) => {
                console.log(err);
            });
    }
    render() {
        return this.state.isLoading ? (
            <div className="App">
                <Header onSearchQuery={this.handleSearchQuery} />
                <Loader />
            </div>
        ) : (
            <div className="App">
                <Header onSearchQuery={this.handleSearchQuery} />
                <SearchFilter
                    filterChanged={this.handleFilter}
                    filterType={this.state.myFilter}
                />
                <Articles articles={this.state.articlesArray} />
            </div>
        );
    }
}

export default App;
